/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>

#include "task.h"
#include "use_case.h"

void* task_executor(void* ptask)
{
    int rc;
    task_t* __ptask = (task_t*) ptask;
    long long count = 0LL;
    task_result_t* presult = (task_result_t*) malloc(sizeof(task_result_t));
    
    ASSERT_NN(__ptask)
    ASSERT_NN(__ptask->task_run)
            
    presult->task_id = __ptask->task_id;
    presult->task_arg_copy_before = __ptask->task_arg_clone_function(__ptask->task_arg);
            
    rc = gettimeofday(&(presult->start_time), NULL);
    ASSERT_ZE(rc);
    
    while (__ptask->limit <= 0 || count < __ptask->limit) {
        __ptask->task_run(__ptask->task_arg);
        count++;
    }
    
    rc = gettimeofday(&(presult->end_time), NULL);
    ASSERT_ZE(rc);
    
    presult->task_arg_copy_after = __ptask->task_arg_clone_function(__ptask->task_arg);
    
    presult->count = count;

    return presult;
}

