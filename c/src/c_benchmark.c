/*
 * Copyright (c) 2015-2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include "use_case.h"
#include "task.h"
#include "c_tasks.h"

#define PRINT_TITLE "%s, routine %p\n"
#define PRINT_HEADER "%7s |%7s |%12s |%12s |%12s |%12s |%12s |%12s |%16s\n", \
                     "n th", "th id", "parg", "arg", "before", "after", "cnt", "time diff/s", "teps"
#define PRINT_FORMAT "%7zd |%7ld |%12p |%12lld |%12lld |%12lld |%12lld |%#12.3f |%#'16.2f\n"

#define SUMMARY_TITLE "%s\n"
#define SUMMARY_HEADER "%s\t%s\t%s\t%s\t%s\n", \
                       "task_exec/s", "value/after", "time/s", "thread/id", "n/threads"
#define SUMMARY_FORMAT "%#'.2f\t%lld\t%#.3f\t%ld\n"
#define SUMMARY_TOTAL "%#'.2f\t\t\t\t%zd\n"

extern long long iterations;

static int direct_execution = 0;
static int summary = 0;

static const char* cmdopts_desc[] = {
    "Prints command line help",
    "The number of iterations for each task / thread",
    "Outputs only a summary (usable in generating graphs)",
    "Direct execution (in the current thread, not in new threads)",
};

static const struct option cmdopts[] = {
    /* name, has_arg, flag, val*/
    {"help", no_argument, NULL, 'h'},
    {"iterations", required_argument, NULL, 'i'},
    {"summary", no_argument, &summary, 1},
    {"direct", no_argument, &direct_execution, 1},
    {NULL, 0, NULL, 0}
};

void cmd_help()
{
    int i = 0;
    printf("Usage:\n");
    while (cmdopts[i].name != NULL) {
        printf("\t--%s", cmdopts[i].name);
        switch (cmdopts[i].has_arg) {
            case optional_argument:
                printf(" [arg]");
                break;
            case required_argument:
                printf(" arg");
                break;
            case no_argument:
                break;
            default:
                // invalid
                break;
        }
        printf("\t%s\n", cmdopts_desc[i]);
        i++;
    }
    exit(0);
}

void command_line_parser(int argc, char* const* argv)
{
    int cmdopt;
    int cmdopts_idx;
    
    do {
        cmdopt = getopt_long(argc, argv, "hi:sd", cmdopts, &cmdopts_idx);
        switch (cmdopt) {
            case 'h':
                cmd_help();
                break;
            case 'i':
                iterations = atoll(optarg);
                if (iterations <= 0) {
                    iterations = DEFAULT_ITERATIONS;
                    fprintf(stderr, 
                            "wrong iterations argument: %s; setting default: %lld\n", 
                            STRNULL(optarg), iterations);
                }
                break;
            case 's':
                summary = 1;
                break;
            case 'd':
                direct_execution = 1;
                break;
        }
        
    } while (cmdopt != -1);
}

void runner(const task_create_function_t* functions, const size_t functions_len, 
        int free_task_arg, int sync)
{

    task_t* pt[functions_len];
    task_result_t* ptr[functions_len];
    pthread_t th[functions_len];
    size_t i;
    int rc;
    double teps;
    double tteps;

    for (i = 0; i < functions_len; i++) {
        pt[i] = functions[i]((long) i);
    }
    
    if (direct_execution) {
        for (i = 0; i < functions_len; i++) {
            ptr[i] = (task_result_t*) task_executor(pt[i]);
        }
    } else {
        for (i = 0; i < functions_len; i++) {
            rc = pthread_create(&(th[i]), NULL, task_executor, pt[i]);
            ASSERT_ZR(rc)
        }
        for (i = 0; i < functions_len; i++) {
            rc = pthread_join(th[i], (void**) &(ptr[i]));
            ASSERT_ZR(rc)
        }
    }
    
    tteps = 0;
    for (i = 0; i < functions_len; i++) {
        if (functions_len == 1 && i == 0) {
            print_task(pt[i]);
        }

        print_task_result(functions_len, pt[i], ptr[i], sync, &teps);
        tteps += teps;
    
        if (sync) {
            if (free_task_arg) {
                long_volatile_delete((long_volatile_t*) pt[i]->task_arg);
            }
            long_volatile_delete((long_volatile_t*) ptr[i]->task_arg_copy_before);
            long_volatile_delete((long_volatile_t*) ptr[i]->task_arg_copy_after);
        } else {
            if (free_task_arg) {
                long_plain_delete((long_plain_t*) pt[i]->task_arg);
            }
            long_plain_delete((long_plain_t*) ptr[i]->task_arg_copy_before);
            long_plain_delete((long_plain_t*) ptr[i]->task_arg_copy_after);
        }
        
        free(pt[i]);
        free(ptr[i]);
    }
    
    if (summary) {
        printf(SUMMARY_TOTAL, tteps, functions_len);
    }
    
}

int main(int argc, char* const* argv) 
{
    size_t i;
    task_create_function_t lpfa[100];
    size_t nths[] = {1, 2, 3, 4, 8, 100};
    size_t nths_len = 6;

    command_line_parser(argc, argv);
    
    printf("%s starting with\n\titerations: %lld\n\tsummary: %s\n\tdirect: %s\n\n", 
            argv[0], iterations, summary ? "yes" : "no", direct_execution ? "yes" : "no");
    
    for (i = 0; i < 100; i++) {
        lpfa[i] = lp_task_increment_create;
    }
    for (i = 0; i < nths_len; i++) {
        runner(lpfa, nths[i], 0, 0);
    }

    for (i = 0; i < 100; i++) {
        lpfa[i] = lp_task_parallel_increment_create;
    }
    for (i = 0; i < nths_len; i++) {
        runner(lpfa, nths[i], 1, 0);
    }

    for (i = 0; i < 100; i++) {
        lpfa[i] = lv_task_increment_create;
    }
    for (i = 0; i < nths_len; i++) {
        runner(lpfa, nths[i], 0, 1);
    }

    for (i = 0; i < 100; i++) {
        lpfa[i] = lv_task_parallel_increment_create;
    }
    for (i = 0; i < nths_len; i++) {
        runner(lpfa, nths[i], 1, 1);
    }
    
    for (i = 0; i < 100; i++) {
        lpfa[i] = lv_task_increment_plus_create;
    }
    for (i = 0; i < nths_len; i++) {
        runner(lpfa, nths[i], 0, 1);
    }

    for (i = 0; i < 100; i++) {
        lpfa[i] = lv_task_parallel_increment_plus_create;
    }
    for (i = 0; i < nths_len; i++) {
        runner(lpfa, nths[i], 1, 1);
    }

    for (i = 0; i < 100; i++) {
        lpfa[i] = la_task_increment_create;
    }
    for (i = 0; i < nths_len; i++) {
        runner(lpfa, nths[i], 0, 1);
    }

    for (i = 0; i < 100; i++) {
        lpfa[i] = la_task_parallel_increment_create;
    }
    for (i = 0; i < nths_len; i++) {
        runner(lpfa, nths[i], 1, 1);
    }
    
    return (EXIT_SUCCESS);
}

void print_task(task_t* ptask)
{
    char buff[1024];
    
    if (summary) {
        printf(SUMMARY_TITLE, STRNULL(ptask->name));
        printf(SUMMARY_HEADER);
    } else {
        int len = snprintf(buff, 1024, PRINT_HEADER);
        int i;

        for (i = 0; i < len - 1; i++) {
            buff[i] = '-';
        }

        printf("%s", buff);
        printf(PRINT_TITLE, STRNULL(ptask->name), ptask->task_run);
        printf("%s", buff);
        printf(PRINT_HEADER);
        printf("%s", buff);
    }
}

long_t get_task_arg_value(void* task_arg, int sync)
{
    long_t value;

    if (task_arg != NULL) {
        
        if (sync) {
            value = long_volatile_get((long_volatile_t *) task_arg);
        } else {
            value = ((long_plain_t *) task_arg)->value;
        }
        
    } else {
        value = -1LL;
    }

    return value;
}

void print_task_result(size_t n, task_t* ptask, task_result_t* ptask_result, int sync, double* pteps)
{

    double teps;
    long long time_diff_us;
    
    time_diff_us = (ptask_result->end_time.tv_sec - ptask_result->start_time.tv_sec) * 1000000LL
            + (ptask_result->end_time.tv_usec - ptask_result->start_time.tv_usec);
    
    teps = (double) ptask_result->count / ((double) time_diff_us / 1000000.0);
    
    if (summary) {
        printf(SUMMARY_FORMAT,
                teps,
                get_task_arg_value(ptask->task_arg, sync),
                (double) time_diff_us / 1000000.0,
                ptask->task_id);
    }
    else {
        printf(PRINT_FORMAT, 
                n, 
                ptask->task_id, 
                ptask->task_arg,
                get_task_arg_value(ptask->task_arg, sync),
                get_task_arg_value(ptask_result->task_arg_copy_before, sync),
                get_task_arg_value(ptask_result->task_arg_copy_after, sync),
                ptask_result->count,
                (double) time_diff_us / 1000000.0,
                teps);
    }
    
    if (pteps != NULL) {
        *pteps = teps;
    }
    
}

