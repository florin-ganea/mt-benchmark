/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef UTIL_H
#define	UTIL_H

#include <stdio.h>
#include <string.h>
#include <errno.h>

#ifdef	__cplusplus
extern "C" {
#endif

#define __unused__ __attribute__((unused)) 
    
#define STRNULL(str)   (str == NULL) ? "(null)" : str

// assert not NULL
#define ASSERT_NN(ptr) \
    if ((ptr) == NULL) {\
        fprintf(stderr, "%s:%d - null pointer\n", __FILE__, __LINE__);\
        abort();\
    }

// assert not zero
#define ASSERT_NZ(num) \
    if ((num) == 0) {\
        fprintf(stderr, "%s:%d - zero\n", __FILE__, __LINE__);\
        abort();\
    }

// assert zero
#define ASSERT_Z(num)  \
    if ((num) != 0) {\
        fprintf(stderr, "%s:%d - non zero [%d]\n", __FILE__, __LINE__, (num));\
        abort();\
    }

// assert zero; non-zero values are error codes
#define ASSERT_ZR(num) \
    if ((num) != 0) {\
        fprintf(stderr, "%s:%d - non zero #%d: %s\n", __FILE__, __LINE__, (num), strerror(num));\
        abort();\
    }

// assert zero; non-zero means failure and errno is set
#define ASSERT_ZE(num) \
    if ((num) != 0) {\
        fprintf(stderr, "%s:%d - non zero [%d] #%d: %s\n", __FILE__, __LINE__, (num), errno, strerror(errno));\
        abort();\
    }

#ifdef	__cplusplus
}
#endif

#endif	/* UTIL_H */
