/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef USE_CASE_H
#define	USE_CASE_H

#include <pthread.h>

#include "util.h"

typedef long long long_t;

#define SELF_LOCK \
    {\
        int __lock_rc;\
        ASSERT_NN(self)\
        __lock_rc = pthread_mutex_lock(&(self->lock));\
        ASSERT_ZR(__lock_rc);

#define SELF_UNLOCK \
        __lock_rc = pthread_mutex_unlock(&(self->lock));\
        ASSERT_ZR(__lock_rc);\
    }

#ifdef	__cplusplus
extern "C" {
#endif

    // simple long
    
    typedef struct long_plain {
        
        long_t value;
        
    } long_plain_t;
    
    long_plain_t* long_plain_new(long_t initial);
    long_plain_t* long_plain_clone(long_plain_t* self);
    void long_plain_delete(long_plain_t* obj);
    
    long_t long_plain_get(long_plain_t* self);
    void   long_plain_set(long_plain_t* self, long_t update);
    
    long_t long_plain_increment_and_get(long_plain_t* self);

    // volatile long
    
    typedef struct long_volatile {
        
        long_t value;
        pthread_mutex_t lock;        
        
    } long_volatile_t;
    
    long_volatile_t* long_volatile_new(long_t initial);
    long_volatile_t* long_volatile_clone(long_volatile_t* self);
    void long_volatile_delete(long_volatile_t* obj);
    
    long_t long_volatile_get(long_volatile_t* self);
    void   long_volatile_set(long_volatile_t* self, long_t update);
    
    long_t long_volatile_increment_and_get(long_volatile_t* self);
    
    // atomic long

    typedef long_volatile_t long_atomic_t;
    
    long_atomic_t* long_atomic_new(long_t initial);
    long_atomic_t* long_atomic_clone(long_atomic_t* self);
    void long_atomic_delete(long_atomic_t* obj);
    
    long_t long_atomic_get(long_atomic_t* self);
    void   long_atomic_set(long_atomic_t* self, long_t update);
    int    long_atomic_compare_and_set(long_atomic_t* self, long_t expect, long_t update);
    
    long_t long_atomic_increment_and_get(long_atomic_t* self);

#ifdef	__cplusplus
}
#endif

#endif	/* USE_CASE_H */
