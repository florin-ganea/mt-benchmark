/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>

#include "use_case.h"

// simple long

long_plain_t* long_plain_new(long_t initial)
{
    long_plain_t* p = (long_plain_t*) malloc(sizeof(long_plain_t));
    ASSERT_NN(p)
    
    p->value = initial;
    
    return p;
}

long_plain_t* long_plain_clone(long_plain_t* self)
{
    return long_plain_new(long_plain_get(self));
}

void long_plain_delete(long_plain_t* obj)
{
    ASSERT_NN(obj)
    free(obj);
}

long_t long_plain_get(long_plain_t* self)
{
    ASSERT_NN(self)
    return self->value;
}

void long_plain_set(long_plain_t* self, long_t update)
{
    ASSERT_NN(self)
    self->value = update;
}

long_t long_plain_increment_and_get(long_plain_t* self)
{
    ASSERT_NN(self)
    return ++(self->value);
}

// volatile long

long_volatile_t* long_volatile_new(long_t initial)
{
    int rc;
    
    long_volatile_t* p = (long_volatile_t*) malloc(sizeof(long_volatile_t));
    ASSERT_NN(p)
    
    p->value = initial;
    
    rc = pthread_mutex_init(&(p->lock), NULL);
    ASSERT_ZR(rc)
    
    return p;
}

long_volatile_t* long_volatile_clone(long_volatile_t* self)
{
    return long_volatile_new(long_volatile_get(self));
}

void long_volatile_delete(long_volatile_t* obj)
{
    int rc;
    ASSERT_NN(obj)
    
    rc = pthread_mutex_destroy(&(obj->lock));
    ASSERT_ZR(rc);
    
    free(obj);
}

long_t long_volatile_get(long_volatile_t* self)
{
    long_t ret;

    SELF_LOCK
            
    ret = self->value;
    
    SELF_UNLOCK
    
    return ret;
}

void long_volatile_set(long_volatile_t* self, long_t update)
{
    SELF_LOCK
    
    self->value = update;
    
    SELF_UNLOCK
}

long_t long_volatile_increment_and_get(long_volatile_t* self)
{
    long_t ret;

    SELF_LOCK
    
    ret = ++self->value;

    SELF_UNLOCK
    
    return ret;
}

// atomic long

long_atomic_t* long_atomic_new(long_t initial)
{
    int rc;
    
    long_atomic_t* p = (long_atomic_t*) malloc(sizeof(long_atomic_t));
    ASSERT_NN(p)
    
    p->value = initial;
    
    rc = pthread_mutex_init(&(p->lock), NULL);
    ASSERT_ZR(rc)
    
    return p;
}

long_atomic_t* long_atomic_clone(long_atomic_t* self) 
{
    return long_atomic_new(long_atomic_get(self));
}

void long_atomic_delete(long_atomic_t* obj)
{
    int rc;
    
    ASSERT_NN(obj)
    
    rc = pthread_mutex_destroy(&(obj->lock));
    ASSERT_ZR(rc);
    
    free(obj);
}

long_t long_atomic_get(long_atomic_t* self)
{
    long_t ret;

    SELF_LOCK
    
    ret = self->value;

    SELF_UNLOCK
    
    return ret;
}

void long_atomic_set(long_atomic_t* self, long_t update)
{
    SELF_LOCK
    
    self->value = update;
    
    SELF_UNLOCK
}

int long_atomic_compare_and_set(long_atomic_t* self, long_t expect, long_t update)
{
    
    return __sync_bool_compare_and_swap(&(self->value), expect, update);
            
}

// java-like implementation of atomic increment and get
long_t long_atomic_increment_and_get(long_atomic_t* self)
{
    long_t current;
    long_t next;
    
    do {
        current = long_atomic_get(self);
        next = current + 1;
    }
    while (long_atomic_compare_and_set(self, current, next) == 0);

    return next;
}
