/*
 * Copyright (c) 2015-2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>

#include "use_case.h"
#include "c_tasks.h"

long long iterations = DEFAULT_ITERATIONS;

// simple counter task

static long_plain_t lp = { 0LL };
static long_volatile_t lv = { 0LL, PTHREAD_MUTEX_INITIALIZER };
static long_volatile_t lv2 = { 0LL, PTHREAD_MUTEX_INITIALIZER };
static long_atomic_t la = { 0LL, PTHREAD_MUTEX_INITIALIZER };

void* lp_task_arg_clone(void* ptr)
{
    return long_plain_clone((long_plain_t*) ptr);
}

void* lv_task_arg_clone(void* ptr)
{
    return long_volatile_clone((long_volatile_t*) ptr);
}

void* la_task_arg_clone(void* ptr)
{
    return long_atomic_clone((long_atomic_t*) ptr);
}

static 
void* lp_task_increment(void* arg)
{
    long_plain_t* lpa = (long_plain_t*) arg;
    long_plain_increment_and_get(lpa);
    return lpa;
}

task_t* lp_task_increment_create(long task_id)
{
    task_t* ptask = (task_t*) malloc(sizeof(task_t));
    
    ptask->name = "long plain increment (64bit unprotected ++)";
    ptask->limit = iterations;
    ptask->task_run = lp_task_increment;
    ptask->task_arg = &lp;
    ptask->task_arg_clone_function = lp_task_arg_clone;
    ptask->task_id = task_id;
    
    long_plain_set(&lp, 0LL);
    
    return ptask;
}

task_t* lp_task_parallel_increment_create(long task_id)
{
    task_t* ptask = (task_t*) malloc(sizeof(task_t));
    
    ptask->name = "long plain parallel increment (64bit unprotected ++)";
    ptask->limit = iterations;
    ptask->task_run = lp_task_increment;
    ptask->task_arg = long_plain_new(0LL);
    ptask->task_arg_clone_function = lp_task_arg_clone;
    ptask->task_id = task_id;
    
    return ptask;
}

static 
void* lv_task_increment(void* arg)
{
    long_volatile_t* lva = (long_volatile_t*) arg;
    long_volatile_increment_and_get(lva);
    return lva;
}

static 
void* lv_task_increment_plus(void* arg)
{
    long_volatile_t* lva = (long_volatile_t*) arg;
    // v++ or v = v + 1
    long_volatile_set(lva, long_volatile_get(lva) + 1);
    return lva;
}

task_t* lv_task_increment_create(long task_id)
{
    task_t* ptask = (task_t*) malloc(sizeof(task_t));
    
    ptask->name = "long volatile increment (64bit synchronized ++)";
    ptask->limit = iterations;
    ptask->task_run = lv_task_increment;
    ptask->task_arg = &lv;
    ptask->task_arg_clone_function = lv_task_arg_clone;
    ptask->task_id = task_id;
    
    long_volatile_set(&lv, 0LL);
    
    return ptask;
}

task_t* lv_task_parallel_increment_create(long task_id)
{
    task_t* ptask = (task_t*) malloc(sizeof(task_t));
    
    ptask->name = "long volatile parallel increment (64bit synchronized ++)";
    ptask->limit = iterations;
    ptask->task_run = lv_task_increment;
    ptask->task_arg = long_volatile_new(0LL);
    ptask->task_arg_clone_function = lv_task_arg_clone;
    ptask->task_id = task_id;
    
    return ptask;
}

task_t* lv_task_increment_plus_create(long task_id)
{
    task_t* ptask = (task_t*) malloc(sizeof(task_t));
    
    ptask->name = "long volatile increment plus (64bit x = x + 1, protected read/write)";
    ptask->limit = iterations;
    ptask->task_run = lv_task_increment_plus;
    ptask->task_arg = &lv2;
    ptask->task_arg_clone_function = lv_task_arg_clone;
    ptask->task_id = task_id;

    long_volatile_set(&lv2, 0LL);
    
    return ptask;
}

task_t* lv_task_parallel_increment_plus_create(long task_id)
{
    task_t* ptask = (task_t*) malloc(sizeof(task_t));
    
    ptask->name = "long volatile parallel increment plus (64bit x = x + 1, protected read/write)";
    ptask->limit = iterations;
    ptask->task_run = lv_task_increment_plus;
    ptask->task_arg = long_volatile_new(0LL);
    ptask->task_arg_clone_function = lv_task_arg_clone;
    ptask->task_id = task_id;
    
    return ptask;
}

static 
void* la_task_increment(void* arg)
{
    long_atomic_t* laa = (long_atomic_t*) arg;
    long_atomic_increment_and_get(laa);
    return laa;
}

task_t* la_task_increment_create(long task_id)
{
    task_t* ptask = (task_t*) malloc(sizeof(task_t));
    
    ptask->name = "long atomic increment (64bit spin CAS ++, protected read)";
    ptask->limit = iterations;
    ptask->task_run = la_task_increment;
    ptask->task_arg = &la;
    ptask->task_arg_clone_function = la_task_arg_clone;
    ptask->task_id = task_id;
    
    long_atomic_set(&la, 0LL);

    return ptask;
}

task_t* la_task_parallel_increment_create(long task_id)
{
    task_t* ptask = (task_t*) malloc(sizeof(task_t));
    
    ptask->name = "long atomic parallel increment (64bit spin CAS ++, protected read)";
    ptask->limit = iterations;
    ptask->task_run = la_task_increment;
    ptask->task_arg = long_atomic_new(0LL);
    ptask->task_arg_clone_function = la_task_arg_clone;
    ptask->task_id = task_id;
    
    return ptask;
}
