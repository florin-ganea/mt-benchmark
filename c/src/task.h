/*
 * Copyright (c) 2015-2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TASK_H
#define	TASK_H

#include <sys/time.h>

#include "util.h"
//#include "c_tasks.h"

#ifdef	__cplusplus
extern "C" {
#endif

typedef void* (*task_arg_clone_function_t)(void*);

// a task to be executed and measured
typedef struct task {

    // the name of the task
    const char* name; 
    
    // the maximum number of iterations
    // 0 or less means indefinite (until the run is canceled from the outside)
    long long limit;
    
    // the task running routine
    void* (*task_run)(void*);
    
    // the argument to be passed to the task running routine
    void* task_arg;
    
    task_arg_clone_function_t task_arg_clone_function;
    
    long task_id;
    
} task_t;

// the result of a measured task execution
typedef struct task_result {
    
    // start time of the execution
    struct timeval start_time;
    
    // end time of the execution
    struct timeval end_time;
    
    // how many times the task was executed
    long long count;
    
    long task_id;
    
    void* task_arg_copy_before;

    void* task_arg_copy_after;

} task_result_t;

void* task_executor(void* ptask);

void print_task(task_t* ptask);
void print_task_result(size_t n, task_t* ptask, task_result_t* ptask_result, int sync, double* pteps);

#ifdef	__cplusplus
}
#endif

#endif	/* TASK_H */

