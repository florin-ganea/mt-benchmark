/*
 * Copyright (c) 2015-2016 Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.benchmark.queues;

import org.jptbox.SpinBlockingQueue;
import java.text.MessageFormat;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Florin Ganea
 */
public class BenchmarkQueues {
    
    private long counter;
    private long startTime;
    private long endTime;
    
    // copied from benchmark class
    private double getExecutionTime() {
        double executionTime = endTime - startTime;
        
        if (executionTime < 0L) {
            // benchmark not finished; use the current time as the end time
            executionTime += System.currentTimeMillis();
        }
        
        executionTime /= 1000.0; // seconds
        
        return executionTime;
    }
    
    // copied from benchmark class
    private double getTEPS() {
        double teps;
        double executionTime = getExecutionTime();
        
        if (executionTime > 0L) {
            teps = (double) counter / executionTime;
        } else {
            // irrelevant execution time
            teps = 0.0;
        }
        
        return teps;
    }

    /**
     * Creates nthreads producers and nthreads consumers, all working on the same queue,
     * each producer expected to produce iterations objects, each consumer expected to 
     * consume iterations objects.
     * 
     * @param queue
     * @param iterations
     * @param nthreads 
     */
    public void execute(BlockingQueue<Product> queue, long iterations, int nthreads) {
        Producer[] producers = new Producer[nthreads];
        Consumer[] consumers = new Consumer[nthreads];

        ObjectProductFactory.resetStatistics();
        ObjectProduct.resetStatistics();
        
        // create
        for (int i = 0; i < nthreads; i++) {
            producers[i] = new Producer(new ObjectProductFactory(), queue, iterations);
            consumers[i] = new Consumer(queue, iterations);
        }
        
        // start
        startTime = System.currentTimeMillis();
        for (int i = 0; i < nthreads; i++) {
            producers[i].start();
            consumers[i].start();
        }
        
        // wait to finish
        for (int i = 0; i < nthreads; i++) {
            try {
                producers[i].join();
                consumers[i].join();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }        
        endTime = System.currentTimeMillis();

        // get the consumed count; throughput is actually decided from this number
        counter = ObjectProduct.getStatistics();
    }

    private void executeRound(String description, BlockingQueue<Product> queue, long iterations, int nthreads) {
        MessageFormat beforeMF = new MessageFormat("{0} [{1} thread(s)]...");
        MessageFormat afterMF = new MessageFormat("done in {0,number,##0.00}s; throughput = {1,number,##0.00} o/s");

        //DecimalFormat df = new DecimalFormat("#,##0.00");
        //System.out.print(description + "\t[" + nthreads + " thread(s)]... ");
        System.out.print(beforeMF.format(new Object[]{description, nthreads})); 
        this.execute(queue, iterations, nthreads);
        //System.out.println("done in \t" + df.format(this.getExecutionTime()) 
        //        + "s; throughput = \t" + df.format(this.getTEPS()) + " o/s");
        System.out.println(afterMF.format(new Object[]{this.getExecutionTime(), this.getTEPS()}));
    }        
    
    public static void main(String[] args) {

        String sdesc = "spinning sleep queue";
        String cdesc = "linked blocking queue";
        
        long iterations = 10_000_000L; // per thread
        
        BlockingQueue<Product> squeue = new SpinBlockingQueue<>(20, TimeUnit.MILLISECONDS);
        BlockingQueue<Product> cqueue = new LinkedBlockingQueue<>();

        BenchmarkQueues bm = new BenchmarkQueues();

        int nthreads[] = new int[]{1, 2, 3, 4, 8, 100};

        System.out.println("----");
        bm.executeRound("warming up -- " + cdesc, cqueue, 1_000_000L, 10);
        System.out.println("--");
        for (int nth : nthreads) {
            bm.executeRound(cdesc, cqueue, iterations, nth);
        }

        System.out.println("--");
        bm.executeRound("warming up -- " + sdesc, squeue, 1_000_000L, 10);
        System.out.println("--");
        for (int nth : nthreads) {
            bm.executeRound(sdesc, squeue, iterations, nth);
        }
        System.out.println("----");
        
    }
    
}
