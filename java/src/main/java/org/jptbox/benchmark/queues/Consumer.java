/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.benchmark.queues;

import java.util.concurrent.BlockingQueue;

/**
 * Product consumer thread -- takes products from a shared queue and consumes them.
 * <p/>
 * <i>Note:</i> It has a fixed number of iterations.
 *
 * @author Florin Ganea
 */
public class Consumer extends Thread {

    private final BlockingQueue<Product> queue;
    private final long count;
    
    public Consumer(BlockingQueue<Product> queue, long count) {
        this.queue = queue;
        this.count = count;
        
        if (queue == null) {
            throw new IllegalArgumentException("queue is null");
        }
    }
    
    @Override
    public void run() {
        boolean running = true;
        long lcount = 0L;
        
        while (running && lcount < count) {
            try {
                Product p = queue.take();
                p.consume();
                lcount++;
            } catch (InterruptedException ex) {
                running = false;
            }
        }
    }
    
}
