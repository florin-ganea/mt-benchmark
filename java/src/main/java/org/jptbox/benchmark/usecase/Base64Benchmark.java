/*
 * Copyright (c) 2015-2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.benchmark.usecase;

import java.util.concurrent.ThreadLocalRandom;
import org.jptbox.codec.Base64;

/**
 * Base64 random byte array encoding.
 *
 * @author Florin Ganea
 */
public class Base64Benchmark implements BenchmarkUseCase {
    
    public enum Base64BenchmarkType {
        ENCODE,
        DECODE,
        TRANSCODE
    }

    public static final int DEFAULT_MAX_SIZE = 1000; // max byte array size
    public static final int DEFAULT_SAMPLES = 1000;  // predefined random samples

    private static final Base64 codec;
    
    static {
        codec = new Base64();
    }
    
    private final int maxSize;
    private final int samples;
    private final Base64BenchmarkType type;
    
    private final byte[][] dataSamples;
    private final char[][] dataEncodings;
    
    private int unsafeSequence;
    
    {
        this.unsafeSequence = 0;
    }
    
    public Base64Benchmark(Base64BenchmarkType btype) {
        this(btype, DEFAULT_MAX_SIZE, DEFAULT_SAMPLES);
    }
    
    public Base64Benchmark(Base64BenchmarkType btype, int maxSize, int samples) {
        this.maxSize = maxSize;
        this.samples = samples;
        this.type = btype;
        
        // random byte arrays
        this.dataSamples = new byte[this.samples][];
        this.dataEncodings = new char[this.samples][];
        ThreadLocalRandom rnd = ThreadLocalRandom.current();
        
        for (int i = 0; i < this.samples; i++) {
            int dataLen = rnd.nextInt(this.maxSize);
            this.dataSamples[i] = new byte[dataLen];
            rnd.nextBytes(this.dataSamples[i]);
            this.dataEncodings[i] = codec.encode(this.dataSamples[i]);
        }
    }

    @Override
    public void run() {
        char[] enc;
        byte[] data;
        
        int idx = this.unsafeSequence;
        if (this.unsafeSequence < this.samples - 1) {
            this.unsafeSequence++;
        } else {
            this.unsafeSequence = 0;
        }

        switch (this.type) {
            case ENCODE:
                data = this.dataSamples[idx];
                enc = codec.encode(data);
                break;
            case DECODE:
                enc = this.dataEncodings[idx];
                data = codec.decode(enc);
                break;
            case TRANSCODE:
                data = this.dataSamples[idx];
                enc = codec.encode(data);
                data = codec.decode(enc);
                break;
        }
        
        // safety check
        /*
        if (data.length == this.dataSamples[idx].length) {
            for (int i = 0; i < data.length; i++) {
                if (data[i] != this.dataSamples[idx][i]) {
                    System.err.println("decoding does not match the initial byte array");
                }
            }
        } else {
            System.err.println("data length " + data.length 
                    + " is not as expected " + this.dataSamples[idx].length);
        }
        */
    }

    @Override
    public String getName() {
        String name = "Base64";
        switch (this.type) {
            case ENCODE:
                name += " encoding";
                break;
            case DECODE:
                name += " decoding";
                break;
            case TRANSCODE:
                name+= " transcoding";
        }
        return name;
    }

    @Override
    public long getValue() {
        return 0L;
    }
    
    public static void main(String[] args) {
        BenchmarkRunner.execute(new Base64Benchmark(Base64BenchmarkType.ENCODE));        
        BenchmarkRunner.execute(new Base64Benchmark(Base64BenchmarkType.DECODE));
        BenchmarkRunner.execute(new Base64Benchmark(Base64BenchmarkType.TRANSCODE));
    }
    
}
