/*
 * Copyright (c) 2015-2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.benchmark.usecase;

import java.text.DecimalFormat;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.jptbox.benchmark.Benchmark;
import org.jptbox.benchmark.BenchmarkProgress;
import org.jptbox.benchmark.BenchmarkStatus;
import org.jptbox.benchmark.BenchmarkType;

/**
 *
 * @author Florin Ganea
 */
public class BenchmarkRunner {
    
    private static final String APP_NAME = BenchmarkRunner.class.getSimpleName();
    private static final String APP_VERSION = "1.0.0";
    
    private static BenchmarkType btype = BenchmarkType.ITERATED;
    private static long runtime = 10_000L; // benchmark running time; 10s
    private static long iterations = 100_000_000L; // benchmark iterations; 100M
    private static int mt = 0;
    private static boolean clone = false;
    private static boolean summary = false;
    private static boolean runAll = false;
    
    private static void printHelp(Options options) {
        new HelpFormatter().printHelp(
                APP_NAME,
                "---- header ----",
                options,
                "---- footer ----",
                true
        );
    }
    
    private static void parseCommandLine(String[] args) {
        
        Option timed = new Option("t", "timed", true, "Timed benchmark.");
        timed.setArgName("milliseconds");
        Option iterated = new Option("i", "iterated", true, "Iterated benchmark");
        iterated.setArgName("count");
        
        OptionGroup gtype = new OptionGroup();
        gtype.addOption(timed);
        gtype.addOption(iterated);
        gtype.setRequired(false);
        
        Option multithreaded = new Option("m", "multithreaded", true, "Multithreaded benchmark.");
        multithreaded.setArgName("threads");
        Option oclone = new Option("c", "clone", false, "Whether the multithreaded benchmark use the same object or a clone in each thread.");
        Option osummary = new Option("s", "summary", false, "Display information in a summary report directly usable in creating graphs");
        Option oall = new Option("a", "all", false, "Run all benchmarks with 1, 2, 3, 4, 8 and 100 threads");
        Option help = new Option("h", "help", false, "Show help.");
        Option version = new Option("v", "version", false, "Show version.");
        
        Options options = new Options();
        options.addOptionGroup(gtype);
        options.addOption(multithreaded);
        options.addOption(oclone);
        options.addOption(osummary);
        options.addOption(oall);
        options.addOption(help);
        options.addOption(version);
        
        try {
            CommandLine cmdl = new DefaultParser().parse(options, args);
            
            if (cmdl.hasOption("v")) {
                System.out.println(APP_NAME + " v" + APP_VERSION);
                System.exit(0);
            }
            
            if (cmdl.hasOption("h")) {
                printHelp(options);
                System.exit(0);
            }
            
            if (cmdl.hasOption("t")) {
                btype = BenchmarkType.TIMED;
                runtime = Long.parseLong(cmdl.getOptionValue("t"));
            }
            
            if (cmdl.hasOption("i")) {
                btype = BenchmarkType.ITERATED;
                iterations = Long.parseLong(cmdl.getOptionValue("i"));
            }
            
            if (cmdl.hasOption("c")) {
                clone = true;
            }
            
            if (cmdl.hasOption("s")) {
                summary = true;
            }
            
            if (cmdl.hasOption("a")) {
                runAll = true;
            }

            if (cmdl.hasOption("m")) {
                mt = Integer.parseInt(cmdl.getOptionValue("m"));
            }

        } catch (Exception ex) {
            // invalid command line
            System.err.println("Invalid command line: " + ex.getMessage());
            printHelp(options);
            System.exit(1);
        }
        
    }

    public static void execute(BenchmarkUseCase uc) {
        execute(uc, clone, mt, true);
    }
    
    public static void execute(BenchmarkUseCase uc, boolean clone, int nthreads, boolean printHeader) {
        long units = iterations;
        
        if (btype == BenchmarkType.TIMED) {
            units = runtime;
        }
        
        DecimalFormat df;
        
        if (summary) {
            df = new DecimalFormat("0.00");
        } else {
            df = new DecimalFormat("#,##0.00");
        }

        if (nthreads > 0) {
            int threadCount = nthreads;
            Runnable[] tasks = new Runnable[threadCount];            
            BenchmarkUseCase ucc = null;
            
            boolean first = true;
            for (int i = 0; i < threadCount; i++) {
                try {
                    if (clone && !first) {
                        ucc = uc.getClass().newInstance();
                    } else {
                        ucc = uc;
                        first = false;
                    }
                } catch (InstantiationException | IllegalAccessException ex) {
                    ex.printStackTrace();
                    ucc = uc;
                }
                tasks[i] = ucc;
            }

            BenchmarkStatus[] ateps = Benchmark.benchmarkMT(tasks, btype, units);
            
            double gteps = 0.0;
            if (summary && ucc != null) {
                if (printHeader) {
                    System.out.println(ucc.getName() + (clone ? " parallel" : ""));
                    System.out.println("task_exec/s\tvalue/after\ttime/s\tthread/id\tn/threads");
                }
            } else {
                System.out.println("----");
            }
            for (int i = 0; i < threadCount; i++) {
                ucc = (BenchmarkUseCase) tasks[i];
                double teps = Benchmark.getTEPS(ateps[i]);
                double tdiff = Benchmark.getExecutionTime(ateps[i]);
                long cnt = ucc.getValue();
                if (summary) {
                    System.out.println(df.format(teps) + "\t" 
                            + String.valueOf(cnt) + "\t"
                            + df.format(tdiff) + "\t"
                            + String.valueOf(i));
                } else {
                    System.out.println(ucc.getName() + (clone ? " parallel" : "") 
                            + " benchmark [" 
                            + String.valueOf(i) 
                            + "]: " 
                            + df.format(teps) + " TEPS"
                            + " (count = " + cnt + ", "
                            + " time diff = " + df.format(tdiff) + ")");
                }
                gteps += teps;
            }
            if (summary) {
                System.out.println(df.format(gteps) + "\t\t\t\t" + threadCount);
            } else {
                System.out.println("----\nTOTAL: " + df.format(gteps) + " TEPS");
            }
        } else {
            double teps;
            teps = BenchmarkProgress.benchmark(uc.getName() + " benchmark... ", uc, btype, units, 80);
            System.out.println("\n" + df.format(teps) + " TEPS");
        }
    }
    
    public static void execute(Class<? extends BenchmarkUseCase> clazz, boolean clone, int nthreads, boolean printHeader) 
            throws InstantiationException, IllegalAccessException {
        execute(clazz.newInstance(), clone, nthreads, printHeader);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Class<? extends BenchmarkUseCase>[] aclazz = new Class[] {
            PrivateIncBenchmark.class, 
            SyncIncBenchmark.class, 
            VolatileIncBenchmark.class, 
            AtomicIncBenchmark.class, 
        };
        
        parseCommandLine(args);
        
        try {
            // warming up...
            execute(EmptyBenchmark.class, false, mt, true);
            // executors...
            if (runAll) {
                int nth[] = {1, 2, 3, 4, 8, 100};
                for (Class<? extends BenchmarkUseCase> clazz : aclazz) {
                    for (int nt : nth) {
                        execute(clazz, false, nt, nt == 1);
                    }
                    for (int nt : nth) {
                        execute(clazz, true, nt, nt == 1);
                    }
                }
            } else {
                for (Class<? extends BenchmarkUseCase> clazz : aclazz) {
                    execute(clazz, false, mt, true);
                    if (clone) {
                        execute(clazz, true, mt, true);
                    }
                }
            }
        } catch (InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }
    
}
