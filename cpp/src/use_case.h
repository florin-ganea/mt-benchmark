/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef USE_CASE_H
#define	USE_CASE_H

#include <mutex>

typedef long long long_t;

class long_plain {
    
protected:
    long_t value;
    
public:
    long_plain(long_t initial) : value(initial) {}
    long_plain(const long_plain& orig) : value(orig.value) {}
    virtual ~long_plain() {}
    
    virtual long_t get() const { 
        return this->value; 
    }
    
    virtual void set(const long_t update) { 
        this->value = update; 
    }
    
    virtual long_t increment_and_get() { 
        return ++this->value; 
    }
    
};

class long_volatile : public long_plain {
    
private:
    std::mutex lock;
    
public:
    long_volatile(long_t initial) : long_plain(initial) {}
    long_volatile(const long_volatile& orig) : long_plain(orig) {}
    virtual ~long_volatile() {}

    virtual long_t get() {
        std::lock_guard<std::mutex> sync(lock);        
        return long_plain::get(); 
    }
    
    virtual void set(long_t update) {
        std::lock_guard<std::mutex> sync(lock);        
        long_plain::set(update); 
    }
    
    virtual long_t increment_and_get() {
        std::lock_guard<std::mutex> sync(lock);
        return long_plain::increment_and_get(); 
    }

};

class long_atomic : public long_volatile {
    
private:
    bool compare_and_set(long_t expect, long_t update);
    
public:
    long_atomic(long_t initial) : long_volatile(initial) {}
    long_atomic(const long_atomic& orig) : long_volatile(orig) {}
    virtual ~long_atomic() {}
    
    virtual long_t increment_and_get();
    
};

#endif	/* USE_CASE_H */
